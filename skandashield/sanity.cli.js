import {defineCliConfig} from 'sanity/cli'

export default defineCliConfig({
  api: {
    projectId: 'bvncdlu7',
    dataset: 'production'
  }
})
