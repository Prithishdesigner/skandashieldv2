/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    extend: {

      backgroundImage:{
        'vector1': "url('/src/Images/titleback/bgco2.jpg')",
        'protop': "url('/src/Images/others/bgtop1.jpg')",
        'vectorproduct': "url('/src/Images/titleback/prodhea.jpg')",
        'serv': "url('/src/Images/titleback/servtit.jpg')",
        'last': "url('/src/Images/others/backsol.png')",
        'solher': "url('/src/Images/others/gradient-digital-transformation-business-background.jpg')",
        'vectorsolution': "url('/src/Images/soluvector.png')",
        'vectordemo': "url('/src/Images/demovector.png')",
        'heroban': "url('/src/Images/others/abstract-techno-background-with-connecting-lines.jpg')",
        'intell': "url('/src/Images/Services/intelligent.png')",
        'light': "url('/src/Images/others/fretgfes.jpg')",
        'aboutus': "url('/src/Images/others/abstract-techno-background-with-connecting-lines.jpg')",
       

        


      }
    },
  },
  plugins: [],
}

