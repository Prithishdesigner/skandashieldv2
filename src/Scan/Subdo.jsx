import React from 'react'
import { useState } from 'react';





export const Subdomains = () => {

  const [formData, setFormData] = useState({
    target: '',
    hostName: '',
    protocol: 'http',
  });

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData((prevData) => ({
      ...prevData,
      [name]: value,
    }));
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    // Add your validation logic here
    console.log('Form submitted:', formData);
  };
  return (
    <>
    
    <div className="bg-[#081226] xl:gap-28 items-center  grid lg:grid-cols-2 xl:grid-cols-2 md:grid-cols-2 sm:grid-cols-2 px-12 xl:px-16 lg:px-20 lg:py-10  xl:py-10  sm:px-10 md:px-16 lg:gap-10">
        {/* <img src={forest} alt="" className="w-full h-full absolute top-0 left-0 object-cover"/> */}
        {/* <img src={myImage} alt="" className="w-full h-48  absolute left-1/3 bottom-20 animate-bounce"/> */}

        <div className="   lg:mt-11 md:mt-6 sm:mt-2">
          <h1 className=" font-extrabold  text-[#79C942] text-2xl lg:text-5xl xl:text-5xl md:text-3xl sm:text-xl lg:mt-5 md:mt-3 ">
          Find the subdomains of an internet domain and determine the attack surface of an organization.
          </h1>
          <h1 className="font-normal tracking-widest text-[10px] lg:text-sm lg:mt-5 md:mt-3 sm:mt-2 mt-3 xl:text-lg sm:text-xs md:text-[10px] text-white">
          This subdomain scanner combines multiple discovery methods and returns only valid results to help you perform extensive reconnaissance.
          </h1>
          <h1 className="font-normal tracking-widest text-[10px] lg:text-sm lg:mt-5 md:mt-3 sm:mt-2 mt-3 xl:text-lg sm:text-xs md:text-[10px] text-white">
          Discover more subdomains and maximize your chances of finding critical vulnerabilities with the built-in discovery methods available through paid plans These also give you access to 20+ security testing tools and features.
          </h1>

          <button className=" lg:font-semibold text-[14px] px-4 py-2 lg:text-sm sm:text-xs lg:px-6 lg:py-4 md:text-[12px] md:px-4 md:py-3 sm:px-3 sm:py-2  rounded-md   text-white bg-[#3C7317]  lg:mt-8 md:mt-4 sm:mt-4 mt-3">
            Book Demo
          </button>
        </div>

        <div className="xl:w-full p-4 bg-gray-100 rounded shadow">
      <form onSubmit={handleSubmit} className="">
      <h1 className=" font-bold  text-[#79C942] text-center text-2xl lg:text-5xl xl:text-2xl md:text-3xl sm:text-xl lg:mt-5 md:mt-3 xl:mb-5">
           Light Scan
          </h1>
        <div className="mb-4">
          <label htmlFor="target" className="block text-gray-700 font-bold mb-2">
            Target:
          </label>
          <input
            type="text"
            id="target"
            name="target"
            placeholder='www.example.com'
            value={formData.target}
            onChange={handleChange}
            required
            className="w-full p-2 border border-gray-300 rounded"
          />
        </div>
   
        
        <div className="text-center ">
        <button
          type="submit"
         className="lg:font-semibold text-[14px] px-4 py-2 lg:text-sm sm:text-xs lg:px-6 lg:py-4 xl:px-5 xl:py-2 md:text-[12px] md:px-4 md:py-3 sm:px-3 sm:py-2  rounded-md   text-white bg-[#3C7317]  lg:mt-8 md:mt-4 sm:mt-4 mt-3"
        >
          Start Scan
        </button>
        </div>
      </form>
    </div>
  
      </div>
    
    
    
    </>
  )
}
